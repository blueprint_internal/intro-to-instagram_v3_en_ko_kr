function BallCombine(target,resources)
{
	this.target = target;
	this.spritesAr = new Array();
	this.resources = resources;
	this.setUpPreloader();
	this.spheres = new Array();
	this.sphereInitialPositionAr = new Array();
	
}

BallCombine.prototype = {
	
	setUpPreloader: function()
	{
		this.keepAspectRatio = true;
		this.loadedGraphics = 0;
		canvas = document.getElementById("ballCombine");
		this.stage = new createjs.Stage(canvas);
		this.stage.enableMouseOver(10);
		borderPadding = 10;

		var barHeight = 20;
		loaderColor = createjs.Graphics.getRGB(61, 92, 155);
		loaderBar = new createjs.Container();

		bar = new createjs.Shape();
		bar.graphics.beginFill(loaderColor).drawRect(0, 0, 1, barHeight).endFill();

		imageContainer = new createjs.Container();
		imageContainer.x = this.resources.width;
		imageContainer.y = this.resources.height;

		loaderWidth = 300;
		this.stage.addChild(imageContainer);

		var bgBar = new createjs.Shape();
		var padding = 3
		bgBar.graphics.setStrokeStyle(1).beginStroke(loaderColor).drawRect(-padding / 2, -padding / 2, loaderWidth + padding, barHeight + padding);

		loaderBar.x = canvas.width - loaderWidth >> 1;
		loaderBar.y = canvas.height - barHeight >> 1;
		loaderBar.addChild(bar, bgBar);

		this.stage.addChild(loaderBar);

		manifest = 
		[
			{src: this.resources.background, id: "background"},
			{src: this.resources.icon_1, id: "icon_1"},
			{src: this.resources.icon_2, id: "icon_2"},
			{src: this.resources.icon_3, id: "icon_3"}
			
		];
		this.map = new Array();
		this.preload = new createjs.LoadQueue(true);
       
		// Use this instead to use tag loading
		//preload = new createjs.PreloadJS(false);

		this.preload.on("progress", this.handleProgress,this);
		this.preload.on("complete", this.handleComplete,this);
		this.preload.on("fileload", this.handleFileLoad,this);
		//"course/en/animations/02_instagram_community/"
		this.preload.loadManifest(manifest,true);

		createjs.Ticker.setFPS(30);
	},
	
	handleProgress: function(event) {
		bar.scaleX = event.loaded * loaderWidth;
	},

	handleComplete: function(event) {
		var background = this.map[0]
		this.stage.addChild(background);
		loaderBar.visible = false;
		
	   	this.setupAnimation();
	   	this.playAnimation();
	    
		
       	this.stage.update();
       	var fn = this.handleTick.bind(this);
       	var self = this;
       	createjs.Ticker.addEventListener("tick",fn);
	    //function handleTick(event) {
	    	//console.log("tick");

	    	//}
	},

    handleTick: function(event){
    	if(this.stage){this.stage.update()};
    },

	handleFileLoad: function(event) {
		this.map.push(new createjs.Bitmap(event.result))
		this.stage.update();
	},

	setupAnimation: function()
	{
		var cnt =Number(this.resources.sphereCount);
		for(var i = 0;i<cnt;i++)
		{
			this.resources["body_"+(i+1)];
			
			var icon = this.map[(i+1)];
				
			var tempSphere = new createjs.Graphics();
			tempSphere.beginFill(createjs.Graphics.getRGB(76,117,171));
			var coverSphere = new createjs.Graphics();
			coverSphere.beginFill(createjs.Graphics.getRGB(76,117,171));
			var body = new createjs.Text(this.resources["sphereText_"+(i+1)], "20px freight-sans-pro","#FFFFFF");
			console.log(body.getMeasuredWidth());
			var useWidth = body.getMeasuredWidth()/2;
			icon.x = -76;
			icon.y = -85;
			body.lineWidth = useWidth-10;
			body.textAlign = "center";
			tempSphere.drawCircle(0,0,(useWidth/2)+20);
			coverSphere.drawCircle(0,0,(useWidth/2)+20);
			var mainSphere = new createjs.Container()
			var tempShape = new createjs.Shape();
			mainSphere.coverSphere = coverSphere;
			tempShape.graphics = tempSphere;
			mainSphere.addChild(tempShape);
			mainSphere.addChild(body);
			mainSphere.addChild(icon);
			var tempShape2 = new createjs.Shape();
			tempShape2.graphics = coverSphere;
			tempShape2.name = "cover";
			mainSphere.addChild(tempShape2);
			this.spheres.push(mainSphere);
			this.stage.addChild(mainSphere);
			mainSphere.y = 200;
			mainSphere.x = this.resources.width/2;
			if(i<1)
			{

				mainSphere.x-=useWidth*1.5;
			}
			else
			{
				mainSphere.x+=useWidth*1.5;
			}
			this.sphereInitialPositionAr.push(mainSphere.x);
			if(i == 1)
			{
				var combineIcon = this.map[3];
				combineIcon.x = -76;
				combineIcon.y = -45;
				combineIcon.name = "combineIcon";
				mainSphere.addChild(combineIcon);
			}

		}
	},
	startAnimation: function()
	{

	},
	resetAnimation: function()
	{
		var fn = this.handleTick.bind(this);
		createjs.Ticker.addEventListener("tick",fn);
		this.startAnimation();
		
	},

	

	playAnimation: function()
	{
		console.log("start animation");
		var cnt =Number(this.resources.sphereCount)
		for(var i = 0;i<cnt;i++)
		{
			var subj = this.spheres[i];
			subj.x = this.sphereInitialPositionAr[i]
			var cover = subj.getChildByName("cover");
			if(i==1)
			{
				var combineLogo = subj.getChildByName("combineIcon");
				combineLogo.alpha = 0;
			}
			cover.alpha = 0;	
				//subj.alpha = 0;
			subj.x = this.sphereInitialPositionAr[i];
			subj.coverSphere.alpha = 0;
			createjs.Tween.get(subj)
				.wait(3000)
				.to({x:this.resources.width/2}, 1000,createjs.Ease.quadInOut)
			createjs.Tween.get(cover)
				.wait(4000)
				.to({alpha:1}, 1000,createjs.Ease.quadInOut)
			if(i==1)
			{
			createjs.Tween.get(combineLogo)
				.wait(5000)
				.to({alpha:1}, 1000,createjs.Ease.quadInOut)
			}
		}
		
		
	},

	animationComplete: function(event)
	{
		
		//createjs.Ticker.on("tick", myFunction);
		//function myFunction(event) {
		   // event.remove();
		//}
		//debugger
		//createjs.Ticker.removeEventListener("tick", handleTick);
		var fn = this.handleTick.bind(this);
       	createjs.Ticker.removeEventListener("tick",self.handleTick);
	},

    
	

	resize: function(w,h)
	{
	
	var ow = this.resources.width; // your stage width
	var oh = this.resources.height; // your stage height

	if (this.keepAspectRatio == true)
	{
	    // keep aspect ratio
	    var scale = Math.min(w / ow, h / oh);
	    this.stage.scaleX = scale;
	    this.stage.scaleY = scale;

	   // adjust canvas size
	   this.stage.canvas.width = ow * scale;
	   this.stage.canvas.height = oh * scale;
	}
	
	this.stage.update()
	//this.replayAnimation();
	}

}
//4c75ab
/*
function addCommas(nStr)
{
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
*/
